const http = require('https');
const fs = require('fs');

const FILE_NAME = "opencv.js";

const resolve = () => {
  fs.readFile(FILE_NAME, function (err, file) {
    if (err) throw err;
    const start = new Date();

    let fileByLine = file.toString().split('\n');
    const from = fileByLine.lastIndexOf('}(this, function () {');
    const to = fileByLine.lastIndexOf('})();');

    fileByLine = fileByLine.slice(from + 1, to + 1)
      .join('\n')
      .concat('\n export default cv;');

    const end = new Date();
    console.log(Math.abs(start - end));

    const OUTPUT_FILE_NAME = `${__dirname}/src/frontend/utility/open-cv.mjs`;
    fs.writeFile(OUTPUT_FILE_NAME, fileByLine, function (err) {
      if (err) {
        return console.log(err);
      }
      console.log("The file was saved!");
    });
  });
};

// potential memory leak if request fails.
const request = http.get('https://docs.opencv.org/master/opencv.js', function(response) {
  const stream = response.pipe(fs.createWriteStream(FILE_NAME));
  stream.on('finish', resolve)
});



