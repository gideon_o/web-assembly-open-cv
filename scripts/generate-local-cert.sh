#!/usr/bin/env bash

openssl req -x509 -out localhost.crt -keyout localhost.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj '/CN=localhost' -extensions EXT -config <( \
   printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")

 mv localhost.crt "$(pwd)/src/backend/security/localhost.crt"
 mv localhost.key "$(pwd)/src/backend/security/localhost.key"
