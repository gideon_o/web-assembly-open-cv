#!/usr/bin/env bash

SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"
UTILITY_PATH="${SCRIPT_PATH}/../src/frontend/utility/"

if [[ $1 = DEV ]]
  then
    touch "${UTILITY_PATH}/open-cv.mjs"
    echo 'console.log("Hello world open-cv");' >> "${UTILITY_PATH}/open-cv.mjs"
fi

if [[ $1 = PROD ]]
  then
    curl https://raw.githubusercontent.com/gideono/build-opencv-js/master/bin/open-cv.mjs --output "${UTILITY_PATH}/open-cv.mjs"
fi
