#!/usr/bin/env bash

SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"

docker build -f "$SCRIPT_PATH/../Dockerfile" --rm -t web-assembly-open-cv .

#
: TODO create bashscript for running in different environment i.e
: environment variables
: server ports
: stop/build/run config etc
: enter running container
#
# TODO
# to run image: docker run -p 3000:3000 -d web-assembly-open-cv
# to enter running image: docker exec -it <container id> /bin/bash
