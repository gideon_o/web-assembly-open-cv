import React, { useEffect } from 'react'
import { getImageData } from '../utility/canvas-support';
import { HEIGHT, TASK_DATA_TYPE_FRAME, WIDTH } from '../utility/constants';
import { start, stop } from '../service/capture';
import Boarder from './Boarder';

/**
 * two issues
 * - not sure that this is a proper custom hook.
 * - tightly coupled with this component*/
const useCapture = (worker, detectedCoordinates) => {

  const [ target, setTarget ] = React.useState(null);

  useEffect(() => {

  }, [detectedCoordinates]);

  const onDataAvailable = async (_) => {
    const image = await getImageData(target).asImageData;
    worker.postMessage({ type: TASK_DATA_TYPE_FRAME, image }, [ image.data.buffer ]);
  };

  return { start: () => start(target, onDataAvailable), setTarget}
};


export default ({ worker, isOpenCVReady, detectedCoordinates }) => {

  const { start, setTarget } = useCapture(worker, detectedCoordinates);

  return (
    <>
      { detectedCoordinates.map((coordinate, i) => <Boarder key={i} coordinate={coordinate} />) }
      <video playsInline width={WIDTH} height={HEIGHT} ref={ (video) => setTarget(video) }/>
      <button disabled={!isOpenCVReady} onClick={ () => start() }> start </button>
      <button onClick={ () => stop() }> stop </button>
    </>
  );
};
