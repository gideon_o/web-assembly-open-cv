import React from 'react'

const generateStyle = ({x: left, y: top, width, height}) => ({
  position: 'absolute',
  width,
  height,
  top,
  left,
  border: 'solid',
});

export default ({ coordinate }) => <span style={ generateStyle(coordinate) }>&nbsp;</span>;

