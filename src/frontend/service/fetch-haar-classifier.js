// TODO implement download for haar classifier files
// await createFileFromUrl('haarcascade_frontalface_default.xml',
//   '/static/data/haarcascade_frontalface_default.xml');
//
// classifier = new cv.CascadeClassifier();
// haar file needs to server as a static resource
// todo use worker to fetch haar file

export default async function(path, url, cv) {
  const res = await self.fetch(url);
  if (!res.ok) {
    throw new Error(`Response is not OK (${res.status} ${res.statusText} for ${url})`);
  }
  const buffer = await res.arrayBuffer();
  const data = new Uint8Array(buffer);
  cv.FS_createDataFile('/', path, data, true, true, false)
}
