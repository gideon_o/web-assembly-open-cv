import React, { Fragment, useEffect, useState } from "react";
import { TASK_DATA_DETECTED, TASK_DATA_TYPE_INIT } from "../utility/constants"
import VideoCapture from '../components/VideoCapture';

export default ({worker}) => {
  const [isInitialized, setIsInitialized] = useState(false);
  const [coordinates, setCoordinates] = useState([]);


  useEffect(() => {
    worker.postMessage({type: TASK_DATA_TYPE_INIT});
  }, []);

  useEffect(() => {
    worker.onmessage = ({ data }) => {
      switch(data.type) {
        case TASK_DATA_TYPE_INIT:
          setIsInitialized(true);
          break;
        case TASK_DATA_DETECTED:
          setCoordinates(data.coordinates.flat());
          break;
      }
    };
  }, []);

  return (
    <Fragment>
      <VideoCapture worker={worker} isOpenCVReady={isInitialized} detectedCoordinates={coordinates}/>
    </Fragment>
  )
}
