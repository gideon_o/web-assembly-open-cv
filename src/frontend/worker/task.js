import {
  TASK_DATA_TYPE_FRAME,
  TASK_DATA_TYPE_INIT } from '../utility/constants';
import * as detect from '../utility/detect';

/*
* TODO
*  - onmessage should handle start, stop, frame (representing data blob) commands to begin with
*  - there is an issue using module import with in this file, error (Window not defined)
*     - to solve import issue, async download of open-cv.mjs may need to moved from detect.js
*       to this file */
self.onmessage = async function ({ data }) {
  switch (data.type) {
    case TASK_DATA_TYPE_INIT:
      detect.init(self.postMessage);
      break;
    case TASK_DATA_TYPE_FRAME:
      detect.find(data.image, self.postMessage);
      break;
    default:
      console.log(`Unknown type was provided: ${data.type
      }`);
  }
};

