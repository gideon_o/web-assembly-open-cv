import   React       from 'react';
import   reactDOM    from 'react-dom';
import { container } from './template/container';
import   Root        from './template/root';
import   init        from './worker/index';



//TODO
// https://docs.opencv.org/3.4/d5/d10/tutorial_js_root.html explicit
// https://github.com/mecab/opencvjs-wasm-webworker-webpack-demo // setup webworker, for facedetection
// - use web works to perform object detection in different "thread"
//  - activate web worker / service worker and export api to control worker
//  - implement face detection see: https://docs.opencv.org/3.4/df/d6c/tutorial_js_face_detection_camera.html
//  - connect face detection with worker
// - setup test for react components
// - how to work with react and dom manipulation resource like WebRTC
// - add sanitized
// - add styled components
reactDOM.render(<Root worker={init()}/>, container());
