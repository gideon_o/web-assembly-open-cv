import {WIDTH, HEIGHT} from "./constants";

let canvas = document.createElement('canvas');

const setDimensions = (width, height) => {
    canvas.width = width, canvas.height = height;
    return canvas;
};

export const canvasContext = (contextType = '2d', contextAttributes = {}, width = WIDTH, height = HEIGHT) =>
    setDimensions(width, height).getContext(contextType, contextAttributes);

export const captureImage = (mimeType = 'image/png', qualityArgument = 1) => new Promise((resolve, _) => {
  canvas.toBlob((blob) => resolve(blob), mimeType, qualityArgument);
});

export const getImageData = (target, dstX = 0, dstY = 0, context = canvasContext()) => {
  context.drawImage(target, dstX, dstY);
  return { asBlob: captureImage, asImageData: context.getImageData(dstX, dstY, WIDTH, HEIGHT) }
};





