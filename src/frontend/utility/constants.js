export const WIDTH    = 640;
export const HEIGHT   = 480;
export const INTERVAL = 200;

export const TASK_DATA_TYPE_INIT = 'init';
export const TASK_DATA_TYPE_FRAME = 'frame';
export const TASK_DATA_DETECTED = 'detected';
