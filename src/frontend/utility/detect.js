// let { default: cv } = await import('../utility/open-cv.mjs');
// async import does not work in web worker context

// import openCV from '../utility/open-cv-1.mjs'
import openCV from './open-cv.mjs'
import { TASK_DATA_DETECTED, TASK_DATA_TYPE_INIT } from './constants';
import createFileFromUrl from '../service/fetch-haar-classifier'

const cv = openCV();
let classifier;

/*
* - kind of a hack, promise should be resolved instead of arbitrary timeout function
* */
export function init(postMessage) {
  return new Promise((resolve, reject) => {
    setTimeout(async () => {
      try {
        classifier = new cv.CascadeClassifier();
        // TODO resolve haar files from own backend
        const url  = 'https://raw.githubusercontent.com/opencv/opencv/master/data/haarcascades/haarcascade_frontalface_alt.xml';
        const path = 'haarcascade_frontalface_alt.xml';
        await createFileFromUrl(path, url, cv);
        const hasLoaded = classifier.load(path);
        console.log(`OpenCv Initialized, classifier loaded ${hasLoaded}`);
        resolve(postMessage({type: TASK_DATA_TYPE_INIT}));
      } catch (e) {
        reject(e)
      }
    }, 1000);
  })
}

export async function find (data, postMessage) {
  const start = new Date();
  const img = cv.matFromImageData(data);
  const imgGray = new cv.Mat();

  let rect = [];
  cv.cvtColor(img, imgGray, cv.COLOR_RGBA2GRAY, 0);
  const faces = new cv.RectVector();
  const msize = new cv.Size(0, 0);
  classifier.detectMultiScale(imgGray, faces, 1.1, 3, 0, msize, msize);

  for (let i = 0; i < faces.size(); i++) {
    rect.push(faces.get(i));
  }

  img.delete();
  faces.delete();
  imgGray.delete();
  const end = new Date();
  // console.log(Math.abs(start - end));

  postMessage({type: TASK_DATA_DETECTED, coordinates: [rect]})
}
