import constraint from "../../src/frontend/utility/rtc-constraint"
describe('RTC Constraint', () => {

  test('should be a configuration object', () => {
    expect(typeof constraint).toBe("object");
  });

  test('should contain video', () => {
    const { video: { width, height } } = constraint;
    expect(typeof width.max).toBe("number");
    expect(typeof height.max).toBe("number");
  });
});



