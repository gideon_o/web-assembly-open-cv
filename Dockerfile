FROM node:11-alpine
WORKDIR /application
COPY . .
RUN npm install
RUN npm run build-open-cv
RUN npm run build

EXPOSE 3000
CMD ["npm", "start"]
